import { browser, by, element } from 'protractor';

export class AppPage {
  constructor() {
    browser.waitForAngularEnabled(false);
  }

  navigateTo(url = '/') {
    return browser.get(url);
  }

  login(username = 'e2e.user@email.com', password = 'e2eUser', sleep = 5000) {
    this.navigateTo('/login');

    element(by.css('input[formControlName="email"]')).sendKeys(username);
    element(by.css('input[formControlName="password"]')).sendKeys(password);
    element(by.css('button[type="submit"]')).click();

    browser.sleep(sleep);
  }

  logout() {
    element(by.cssContainingText('button', 'Logout')).click();
  }

  expectCurrentUrl(urlExpected) {
    browser.getCurrentUrl().then((url) => {
      expect(url).toBe(browser.baseUrl + urlExpected);
    });
  }

  getText(cssSelector) {
    return element(by.css(cssSelector)).getText();
  }

  screenshot(filename) {
    browser.takeScreenshot().then(function (png) {
      let stream = require('fs').createWriteStream(filename);
      stream.write(new Buffer(png, 'base64'));
      stream.end();
    });
  }
}
