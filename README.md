# Budget Monitoring
[![pipeline status](https://gitlab.com/gino0012/budget-monitoring/badges/master/pipeline.svg)](https://gitlab.com/gino0012/budget-monitoring/commits/master) [![coverage report](https://gitlab.com/gino0012/budget-monitoring/badges/master/coverage.svg)](https://gitlab.com/gino0012/budget-monitoring/commits/master)


## Setup

Install [node](https://nodejs.org/en/)

Run in cmd or terminal:

    npm install -g @angular/cli nodemon

## For Developer

Run `npm run develop` for a dev server. Navigate to `http://localhost:4200/`.

## For User

Run `npm run serve` for user. Navigate to `http://localhost:4200/`.

## Running unit tests

Run `npm test` to execute the unit tests

## Running end-to-end tests

Run `npm run serve` and run `npm run e2e` in another terminal.
