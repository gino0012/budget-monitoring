import { Observable } from 'rxjs/Observable';

export class MockAngularFireAuth {
  authState: Observable<{}>;

  auth = {
    signInWithRedirect: jasmine.createSpy('afauth signInWithRedirect'),
    signOut: jasmine.createSpy('afauth signOut'),
    signInWithEmailAndPassword: jasmine.createSpy('afauth signInWithEmailAndPassword')
  };
}
