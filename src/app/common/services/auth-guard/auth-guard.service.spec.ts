import { TestBed, inject } from '@angular/core/testing';

import { AuthGuard } from './auth-guard.service';
import { AuthService } from '../auth/auth.service';
import { Router, RouterStateSnapshot } from '@angular/router';
import { MockAuthService } from '../../test/mocks/mock-auth-service';
import { MockRouter } from '../../test/mocks/mock-router';
import { Observable } from 'rxjs/Observable';

describe('AuthGuard', () => {
  const MOCK_URL = 'sample/url';

  let service: AuthGuard;
  let mockRouter: Router;
  let mockAuthService: MockAuthService;
  let canActivateSuccessSpy, canActivateFailedSpy;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AuthGuard,
        { provide: AuthService, useClass: MockAuthService },
        { provide: Router, useClass: MockRouter }
      ]
    });
  });

  beforeEach(inject([AuthGuard, Router, AuthService], (_service_, _mockRouter_, _mockAuthService_) => {
    service = _service_;
    mockRouter = _mockRouter_;
    mockAuthService = _mockAuthService_;

    canActivateSuccessSpy = jasmine.createSpy('canActivate success');
    canActivateFailedSpy = jasmine.createSpy('canActivate failed');
  }));

  it('should navigate to login if invalid user', () => {
    mockAuthService.user$ = Observable.of(null);

    service.canActivate(null, <RouterStateSnapshot>{url: MOCK_URL})
      .subscribe(canActivateSuccessSpy, canActivateFailedSpy);

    expect(canActivateSuccessSpy).toHaveBeenCalledWith(false);
    expect(mockRouter.navigate).toHaveBeenCalledWith(['/login'], {queryParams: { returnUrl: MOCK_URL}});
  });

  it('should not navigate to login if valid user', () => {
    mockAuthService.user$ = Observable.of({name: 'test name'});

    service.canActivate(null, <RouterStateSnapshot>{url: MOCK_URL})
      .subscribe(canActivateSuccessSpy, canActivateFailedSpy);

    expect(canActivateSuccessSpy).toHaveBeenCalledWith(true);
    expect(mockRouter.navigate).not.toHaveBeenCalled();
  });
});
