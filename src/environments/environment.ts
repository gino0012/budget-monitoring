// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyDXDw375Z-hkNjzYJR2yQWWOnnZxS1o0LQ',
    authDomain: 'budget-monitoring-develop.firebaseapp.com',
    databaseURL: 'https://budget-monitoring-develop.firebaseio.com',
    projectId: 'budget-monitoring-develop',
    storageBucket: 'budget-monitoring-develop.appspot.com',
    messagingSenderId: '712191082251'
  }
};
