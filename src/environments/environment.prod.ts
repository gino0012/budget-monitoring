export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyCEGGkFe6c5Qjp5lGmjp73PZdVclVDIdcs',
    authDomain: 'budget-monitoring.firebaseapp.com',
    databaseURL: 'https://budget-monitoring.firebaseio.com',
    projectId: 'budget-monitoring',
    storageBucket: 'budget-monitoring.appspot.com',
    messagingSenderId: '861770303263'
  }
};
